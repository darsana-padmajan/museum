import Vue from "vue";
import firebase from 'firebase';
import Router from "vue-router";
import Home from "./views/Home.vue";
import Pdf from "./views/Pdf.vue";
import Login from "./views/Login.vue";
import profile from "./views/profile.vue";
import Invitation from "./views/Invitation.vue";
import Profileview from "./views/Profileview.vue";
import SignUp from "./views/SignUp.vue";


Vue.use(Router);

const router = new Router({
  routes: [
     {
      path: "/",
      name: "Home",
      component: Home
    }, 
    {
      path: "/Pdf",
      name: "Pdf",
      component: Pdf,
      meta:{
        requiresAuth:true
      }

    },
    {
      path: "/profile",
      name: "profile",
      component: profile
    },
    {
      path: "/Profileview",
      name: "Profileview",
      component: Profileview,
      props: (route) => ({
        id: route.query.id
       
      })
    },
    {
      path: "/Invitation",
      name: "Invitation",
      component: Invitation,
      props: (route) => ({
        to: route.query.to,
        stage: route.query.stage,
        bg: route.query.bg
    })
    },


    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/Login",
      name: "Login",
      component: Login
    },
    {
      path: "/Home",
      name: "Home",
      component: Home
    },
    {
      path: "/SignUp",
      name: "SignUp",
      component: SignUp
    }
  ]
});
router.beforeEach((to,from,next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  
  if(requiresAuth && !currentUser) next ('login');
  else if (!requiresAuth && currentUser) next ('pdf');
  else next();
  });
  
  export default router;
  
